//
//  SCLoginVC+TextField.swift
//  Shift
//
//  Created by Mobilecoderz1 on 24/06/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//
import UIKit
extension SCLoginVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textFieldCountryCode == textField {
            showCountryList()
            return false
        }
        return true
    }
}
