//
//  SCLoginVC.swift
//  Shift
//
//  Created by Mobilecoderz1 on 24/06/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import UIKit
import KYDrawerController
protocol LoginDismissedDelegate: class {
    func loginDismissed()
}
enum LoginState{
    case insideApp
    case outsideApp
}
class SCLoginVC: MOLHViewController {
    @IBOutlet weak var textFieldCountryCode: UITextField!
    @IBOutlet weak var textFieldPhoneNumber: UITextField!
    var currentLoginState: LoginState = .insideApp
    weak var delegate: LoginDismissedDelegate?
    @IBOutlet weak var buttonSkip: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldCountryCode.delegate = self
        textFieldPhoneNumber.delegate = self
        textFieldCountryCode.text = "+966"
        textFieldPhoneNumber.keyboardType = .numberPad
        addDoneButtonOnKeyboard()
    }
    func showCountryList() {
        let vc = SCCountryPickerVC.instantiate(from: .registration)
        vc.delegate = self
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.shadowImage = UIImage()
        present(navigationController, animated: true, completion: nil)
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barTintColor = UIColor.AppColor.appGreen
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [flexSpace, done]
        done.tintColor = .white
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textFieldPhoneNumber.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        guard let code = textFieldCountryCode.text, !code.isEmpty else {
            DisplayBanner.show(message: "Please enter country code.")
            return
        }
        guard let phoneNumber = textFieldPhoneNumber.text , !phoneNumber.isEmpty else {
            DisplayBanner.show(message: "Please enter phone number.")
            return
        }
        if phoneNumber.count < 8 || phoneNumber.count > 16 {
            DisplayBanner.show(message: "Phone number length should between 8 to 16.")
            return
        }
        if Int(phoneNumber) == nil {
            DisplayBanner.show(message: "Please enter valid phone number.")
            return
        }
        textFieldPhoneNumber.resignFirstResponder()

        let fullNumber = code + " - " + phoneNumber
        switchToVerificationAlert(number: fullNumber)
    }
    @IBAction func buttonLoginTapped(_ sender: UIButton) {
        switch currentLoginState {
            
        case .insideApp:
            dismiss(animated: true, completion: nil)
        case .outsideApp:
            switchToDashboard()
            LocationManager.sharedInstance.initLocation()
        }
     
    }
    @IBAction func buttonSkipTapped(_ sender: UIButton) {
     
    }

    func switchToVerificationAlert(number: String){
        let vc = SCAlertVC.instantiate(from: .popup)
        vc.currentAlertType = .verification
        vc.mobileNumber = number
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    func switchToVerificationVC(phoneNumber: String?, countryCode: String?, code: String?){
        let vc = SCVerificationVC.instantiate(from: .registration)
        vc.delegate = self
        vc.phoneNumber = phoneNumber
        vc.countryCode = countryCode
        vc.code = code
        present(vc, animated: true, completion: nil)
    }
    func switchToDashboard(){
        let screenWidth = UIScreen.main.bounds.width
        let drawerController = KYDrawerController(drawerDirection: MOLHLanguage.isRTLLanguage() ? .right : .left, drawerWidth: screenWidth * 0.75)
        let sideMenuVC = SCSideMenuVC.instantiate(from: .dashboard)
        let mainVC = SCHomeVC.instantiate(from: .dashboard)
        let navigation = SwipeableNavigationController(rootViewController: mainVC)
        navigation.setNavigationBarHidden(true, animated: true)
        drawerController.mainViewController = navigation
        drawerController.drawerViewController = sideMenuVC
        navigationController?.pushViewController(drawerController, animated: true)
    }
    func checkToDismissKeyboard(){
        if textFieldPhoneNumber.isFirstResponder {
            textFieldPhoneNumber.resignFirstResponder()
        }
    }
}
extension SCLoginVC: VerificationDelegate{

    
    func verified() {
        updateProfileDataInsideMenu()
        if sAppDelegate.isProfileCompletedEnoughForBooking() {
            switch currentLoginState {
                
            case .insideApp:
                dismiss(animated: true){[weak self] in
                  self?.delegate?.loginDismissed()// was commented
                }
            case .outsideApp:
                switchToDashboard()
            }
            Constants.sharedAppDelegate.requestForLocation()
        }else{
            forceUserToFillProfile()
        }
     
    }
    func updateProfileDataInsideMenu(){
        if let drawer = ((presentingViewController as? UINavigationController)?.viewControllers.last) as? KYDrawerController{
            let sideVC = drawer.drawerViewController as? SCSideMenuVC
            sideVC?.loginStatusChanged()
        }
    }
    func forceUserToFillProfile(){
        let vc = SCProfileVC.instantiate(from: .setting)
        vc.delegate = self
        vc.currentProfileState = .forcing
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
}
extension SCLoginVC: ProfileDismissedDelegate{
    func profileDismissed(with completion: Bool) {
        switch currentLoginState {
            
        case .insideApp:
            dismiss(animated: true){[weak self] in
                if completion{
                self?.delegate?.loginDismissed()
                }
            }
        case .outsideApp:
            
            if completion {
                switchToDashboard()
            }else{
                switchToDashboard()
            }
        }
        
    }
    
}
extension SCLoginVC: CountryDelegate{
    func selected(countryCode: String) {
        textFieldCountryCode.text = countryCode
    }
}
extension SCLoginVC: AlertDismissDelegate {
    func alertDismissed(type: AlertType, data: Any?) {
        if type == .verification {
            if let fullNumber = data as? String{
                let codeAndNumber = fullNumber.components(separatedBy: " - ")
                if let code = codeAndNumber.first, let number = codeAndNumber.last {
                    let params = [ServerKeys.countryCode: code, ServerKeys.phoneNumber: number, ServerKeys.fullPhoneNumber: number]
                    requestForVerificationCode(parameters: params)
                }
                
            }
        }
      
    }
}
