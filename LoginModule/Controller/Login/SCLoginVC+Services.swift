//
//  SCLoginVC+Services.swift
//  Shift
//
//  Created by Mobilecoderz1 on 27/06/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import Foundation
extension SCLoginVC{
    func requestForVerificationCode(parameters: [String: Any]) {
        ApiManager.requestGenericServer(path: MethodName.requestVerificationCode, parameters: parameters, methodType: .post , result:  { [weak self](model: VerificationCode) in
            self?.executeLoginSuccess(model: model, parameters: parameters)
        }) { (error) in
            
        }
}
    func executeLoginSuccess(model: VerificationCode, parameters: [String: Any]){
//        if let otp = data[ServerKeys.otp] as? Int {
//            DisplayBanner.show(message: "Your otp is \(otp)")
//        }
        if let code = model.otp {
            switchToVerificationVC(phoneNumber: parameters[ServerKeys.phoneNumber] as? String, countryCode: parameters[ServerKeys.countryCode] as? String,code: "\(code)")
        }
      
    }
}
