//
//  SCVerificationVC.swift
//  Shift
//
//  Created by Mobilecoderz1 on 25/06/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import UIKit
enum VerificationStatus {
    case newUser
    case existingUser
}
protocol VerificationDelegate: class {
    func verified()
}
class SCVerificationVC: UIViewController {
    weak var delegate: VerificationDelegate?
    var phoneNumber: String?
    var countryCode: String?
    var code: String?
    var currentVerificationStatus: VerificationStatus = .newUser
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var textFieldCode: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 12.0, *) {
            textFieldCode.textContentType = .oneTimeCode
        }
    }
    func autoFillCode(){
        let subViews = stackView.arrangedSubviews
        
        if code?.count == subViews.count {
            for (index, ch) in code!.enumerated() {
                let textfield = getViewWith(tag: index)
                textfield?.text = String(ch)
            }
        }
    }
    func getViewWith(tag: Int) -> UITextField?{
        let subViews = stackView.arrangedSubviews
        for view in subViews{
            if view.tag == tag {
                return view as? UITextField
            }
        }
        return nil
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
       // dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonCloseTapped(_ sender: UIButton) {
       dismiss(animated: true, completion: nil)
    }
    @IBAction func buttonSubmitTapped(_ sender: UIButton) {
        guard let number = phoneNumber else {
            DisplayBanner.show(message: "Phone number not available.")
            return
        }
        guard let countryCode = countryCode else {
            DisplayBanner.show(message: "Country code not available.")
            return
        }
        guard let otp = textFieldCode.text, !otp.isEmpty else {
            DisplayBanner.show(message: "Please enter code.")
            return
        }
        if textFieldCode.isFirstResponder {
            textFieldCode.resignFirstResponder()
        }
        var params = [ServerKeys.phoneNumber: number,ServerKeys.countryCode: countryCode, ServerKeys.otp: otp]
        if let userId = Constants.userDefaults.string(forKey: UserDefaultKeys.id){
            params[ServerKeys.userId] = userId
        }
        requestVerificationServer(parameters: params)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //getViewWith(tag: 0)?.becomeFirstResponder()
        textFieldCode.becomeFirstResponder()
    }
    @IBAction func buttonResendOtpTapped(_ sender: UIButton) {
        guard let countryCode = countryCode, let phoneNumber = phoneNumber else {
            DisplayBanner.show(message: "Country code or phone number missing.")
            return
        }
         let params = [ServerKeys.countryCode: countryCode, ServerKeys.phoneNumber: phoneNumber]
        requestForVerificationCode(parameters: params)
    }
}

