//
//  SCVerificationVC+Services.swift
//  Shift
//
//  Created by Mobilecoderz1 on 27/06/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import Foundation
extension SCVerificationVC{
    func requestVerificationServer(parameters: [String: Any]) {
        ApiManager.requestGenericServer(path: currentVerificationStatus == .newUser ? MethodName.verifyCode: MethodName.verifyOtp, parameters: parameters, methodType: .post , result:  { [weak self](user: User) in
            self?.executeVerificationSuccess(user: user)
        }) { (error) in
            
        }
    }
    func executeVerificationSuccess(user: User){
        Constants.sharedAppDelegate.saveUser(info: user)
        dismiss(animated: true) {[weak self] in
            self?.delegate?.verified()
        }
    }
    
    func requestForVerificationCode(parameters: [String: Any]) {
        ApiManager.requestGenericServer(path: MethodName.requestVerificationCode, parameters: parameters, methodType: .post , result:  { [weak self](model: VerificationCode) in
            self?.executeForVerificationCodeSuccess(model: model, parameters: parameters)
        }) { (error) in
            
        }
    }
    func executeForVerificationCodeSuccess(model: VerificationCode, parameters: [String: Any]){
        if let code = model.otp {
            self.code = String(code)
           //autoFillCode()
        }
      
    }
}
