//
//  VerificationCode.swift
//  Shift
//
//  Created by Mobilecoderz1 on 30/07/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import Foundation
struct VerificationCode: Decodable {
    let otp: Int?
    let message: String?
}
