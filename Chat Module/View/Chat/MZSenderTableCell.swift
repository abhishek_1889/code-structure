//
//  MZSenderTableCell.swift
//  MyZoo
//
//  Created by Vikas Sachan on 29/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import UIKit

class MZSenderTableCell: UITableViewCell {
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var viewBorder: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
