//
//  MZChatTableCell.swift
//  MyZoo
//
//  Created by Vikas Sachan on 04/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import UIKit

class MZChatTableCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPost: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var buttonMessage: UIButton!
    @IBOutlet weak var labelUnreadCount: UILabel!
    @IBOutlet weak var viewCountCircle: UIView!
    @IBOutlet weak var labelFrom: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
