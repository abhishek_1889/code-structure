//
//  MZReceiverImageTableCell.swift
//  MyZoo
//
//  Created by Mobilecoderz1 on 06/05/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import UIKit

class MZReceiverImageTableCell: UITableViewCell {
    @IBOutlet weak var imageViewImage: UIImageView!
    @IBOutlet weak var labelDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
