//
//  MZChatVC+Services.swift
//  MyZoo
//
//  Created by Mobilecoderz1 on 06/05/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import Foundation
extension MZChatVC{
    func requestServer(params: [String: Any]) {
        ApiManager.requestMultipartApiServer(path: SocketEvents.uploadChatImage, parameters: params, methodType: .post, result: { [weak self](result) in
            switch result {
            case .success(let data):
                self?.handleSuccess(json: data)
            case .failure(let error):
                self?.handleFailure(error: error)
            case .noDataFound(_):
                break
            }
        }) { (progress) in
            Console.log("progress \(progress)")
        }
    }
    func handleSuccess(json: Any){
        if let json = json as? [String : Any]{
            if let data = json["data"] as? [String: Any] {
                if let firstImage = (data[ServerKeys.image] as? Array<String>)?.first {
                    currentMessageType = .imageUrlWithSocket
                    sendToServer(message: "", imageUrl: firstImage)
                }
            }
        }
    }
    
    func handleFailure(error: Error?){
        if let error = error {
            DisplayBanner.show(message: error.localizedDescription)
        }else{
            DisplayBanner.show(message: ErrorMessages.somethingWentWrong)
        }
    }}
