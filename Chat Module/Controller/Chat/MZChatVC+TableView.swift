//
//  MZChatVC+TableView.swift
//  MyZoo
//
//  Created by Vikas Sachan on 26/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import Foundation
import  RealmSwift
extension MZChatVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessages?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = chatMessages?[indexPath.row]
        if message?.senderId == Constants.userDefaults.value(forKey: UserDefaultkeys.userId) as? Int{
            if let chatMessage = message?.message, !chatMessage.isEmpty {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MZSenderTableCell") as? MZSenderTableCell else{return UITableViewCell()}
                cell.labelMessage.text = message?.message
                cell.labelDate.text = getTimeFromTimeStamp(timeStamp: message?.timeStamp)
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MZSenderImageTableCell") as? MZSenderImageTableCell else{return UITableViewCell()}
                cell.imageViewImage.setRadius(10)
                cell.imageViewImage.image = nil
                if let imgeUrl = message?.url, let url = URL(string: imgeUrl){
                    cell.imageViewImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), options: .lowPriority, completed: nil)
                }else{
                    cell.imageViewImage.image = UIImage(named: "placeholder")
                }
                cell.labelDate.text = getTimeFromTimeStamp(timeStamp: message?.timeStamp)
                return cell
            }
            
        }else{
            if let chatMessage = message?.message, !chatMessage.isEmpty {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MZReceiverTableCell") as? MZReceiverTableCell else{return UITableViewCell()}
                cell.viewBorder.setBorder(color: .lightGray, borderWidth: 2, radius: 0)
                cell.labelMessage.text = message?.message
                cell.labelDate.text = getTimeFromTimeStamp(timeStamp: message?.timeStamp)
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MZReceiverImageTableCell") as? MZReceiverImageTableCell else{return UITableViewCell()}
                cell.imageViewImage.setRadius(10)
                cell.imageViewImage.image = nil
                if let imgeUrl = message?.url, let url = URL(string: imgeUrl){
                    cell.imageViewImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"), options: .lowPriority, completed: nil)
                }else{
                    cell.imageViewImage.image = UIImage(named: "placeholder")

                }
                cell.labelDate.text = getTimeFromTimeStamp(timeStamp: message?.timeStamp)
                return cell
            }
            
        }
      
       
    }
    func getTimeFromTimeStamp(timeStamp:String?)-> String? {
        if  let timeStamp = timeStamp,let unixTime =  Double(timeStamp) {
            let date = Date(timeIntervalSince1970: unixTime/1000)
            dateFormatter.dateFormat = "hh:mm a"
            dateFormatter.locale  = Locale.current
            dateFormatter.timeZone = TimeZone.current
            let dateStr = dateFormatter.string(from: date)
            return dateStr
        }
        return nil
    }
    
}
extension MZChatVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let result = chatMessages?[indexPath.row]
        checkToResignTextView()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = chatMessages?[indexPath.row]
        if let chatMessage = message?.message, !chatMessage.isEmpty {
            return UITableView.automaticDimension
        }else{
            return 150
        }
        
    }

}
