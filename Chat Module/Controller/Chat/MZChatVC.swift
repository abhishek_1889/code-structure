//
//  MZChatVC.swift
//  MyZoo
//
//  Created by Vikas Sachan on 26/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import UIKit
import RealmSwift
import GrowingTextView
enum MessageType: Int{
    case textWithSocket
    case imageUrlWithSocket
    case imageToServer
}
class MZChatVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var lastMessageModel: LastMessageModelRealm?
    var chatMessages: Results<ChatMessageModelRealm>?
    var token: NotificationToken?
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var viewMessageBorder: UIView!
    @IBOutlet weak var constraintBottom: NSLayoutConstraint!
    var currentMessageType: MessageType = .textWithSocket
    override func viewDidLoad() {
        super.viewDidLoad()
        Console.log("MZChatVC initialized")
        textView.placeholderLabel.text = localized(value: LanguageKey.typeMessage)
        setupNavigationBar()
        setupTableView()
        viewMessageBorder.setBorder(color: .lightGray, borderWidth: 2, radius: 0)
        getLatestMessagesFromServer(loadLocalMessages: true)
        textView.maxHeight = 90
        textView.text = ""
        registerKeyboardNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadChatMessages), name: .getChatMessageList, object: nil)
    }
    @objc func loadChatMessages() {
        Console.log("MZChatVC should load messages")
        getLatestMessagesFromServer(loadLocalMessages: false)

    }
    func getLatestMessagesFromServer(loadLocalMessages: Bool){
        if let friendId = lastMessageModel?.userId, let animalId = lastMessageModel?.animalId, let type = lastMessageModel?.zooType, let userId = Constants.userDefaults.value(forKey: UserDefaultkeys.userId) as? Int {
            var model = ChatInfo(isChatting: true)
            model.friendId = friendId
            model.userId = userId
            model.animalId = animalId
            SocketHelper.shared.chatInfo = model
            if loadLocalMessages{
            loadMessages(senderId: userId, recieverId: friendId, animalId: animalId)
            }
            var typeId = ""
            if type == 1 {
                typeId = "animalId"
            }else if type == 2 {
                typeId = "accessId"
            }else if type == 3 {
                typeId = "serviceId"
            }
            let lastMessageId = chatMessages?.last?.id ?? 0
            
            let params = [SocketKeys.receiverId: String(friendId), SocketKeys.senderId: String(userId), typeId: String(animalId), SocketKeys.lastMessageId: String(lastMessageId)]
            let paramsForReadAll = [SocketKeys.receiverId: String(userId), SocketKeys.senderId: String(friendId), "animalId": String(animalId)]
            SocketHelper.shared.getAllChatMessages(params: params, readAllParams: paramsForReadAll)
        }
    }
    func setupTableView(){
        tableView.register(UINib(nibName: "MZReceiverTableCell", bundle: nil), forCellReuseIdentifier: "MZReceiverTableCell")
        tableView.register(UINib(nibName: "MZSenderTableCell", bundle: nil), forCellReuseIdentifier: "MZSenderTableCell")
        tableView.register(UINib(nibName: "MZReceiverImageTableCell", bundle: nil), forCellReuseIdentifier: "MZReceiverImageTableCell")
        tableView.register(UINib(nibName: "MZSenderImageTableCell", bundle: nil), forCellReuseIdentifier: "MZSenderImageTableCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 90
        tableView.dataSource = self
        tableView.delegate = self
    }
    func registerKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func checkToResignTextView(){
        if textView.isFirstResponder {
            textView.resignFirstResponder()
        }
    }
    
    func loadMessages(senderId: Int, recieverId: Int, animalId: Int){
        let messages = RealmDatabaseManager.sharedInstance.getChatMessagesFromDatabase(senderId: senderId, recieverId: recieverId, animalId: animalId)
        chatMessages = messages
        token = messages.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
               tableView.reloadData()
               self?.scrollToBottom(animated: false)
                break
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
                Console.log("messages.observe insertions \(insertions) deletions \(deletions) modifications \(modifications)")
                Console.log("messages.observe before offset \(tableView.contentOffset) contentSize : \(tableView.contentSize)")
                if insertions.count > 0 && deletions.count == 0 {
                    self?.scrollToBottom(animated: true)
                    Console.log("messages.observe after offset \(tableView.contentOffset) contentSize : \(tableView.contentSize)")

                }
                
            case .error(let error):
                fatalError("\(error)")
            }
        }
    }
  
    func setupNavigationBar(){
        setLeftBarButtonItem(imageName: "back_icon")
        navigationItem.leftBarButtonItem?.action = #selector(backButtonTapped)
        navigationItem.title = lastMessageModel?.fullName
    }
    @objc func backButtonTapped(){
        navigationController?.popViewController(animated: true)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

          Constants.sharedAppDelegate.setSwipe(enabled: true, navigationController: navigationController)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // scrollToBottom(animated: false)

    }
    deinit {
        token?.invalidate()
        Console.log("MZChatVC deinitialized")
        stoppedChatting()

    }
    @IBAction func buttonSendMessageTapped(button: UIButton){
        currentMessageType = .textWithSocket
        guard let text = textView.text, !text.isEmpty else {
            DisplayBanner.show(message: "Please enter message")
            return
        }
    let trimmedString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedString.isEmpty{
            DisplayBanner.show(message: "Please enter message")
            return
        }
        sendToServer(message: trimmedString)
        textView.text = ""
    }
    @IBAction func buttonAddAttachmentTapped(button: UIButton){
        MediaOption.shared.allowsEditing = true
        MediaOption.shared.showMediaSheetOn(self)
        MediaOption.shared.delegate = self
    }
    func scrollToBottom(animated: Bool){
        if let count = chatMessages?.count, count > 1 {
          tableView.scrollToRow(at: IndexPath(row: count - 1, section: 0), at: .bottom, animated: animated)
        }
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        checkToResignTextView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      
    }
    func stoppedChatting(){
        var model = ChatInfo(isChatting: false)
        model.friendId = nil
        model.userId = nil
        model.animalId = nil
        SocketHelper.shared.chatInfo = model
    }
}
extension MZChatVC{
    func sendToServer(message: String, image: UIImage? = nil, imageUrl: String? = nil){
        if let friendId = lastMessageModel?.userId, let animalId = lastMessageModel?.animalId, let type = lastMessageModel?.zooType, let userId = Constants.userDefaults.value(forKey: UserDefaultkeys.userId) as? Int {
            var typeId = ""
            var forKey = ""
            if type == 1 {
                typeId = "animalId"
                forKey = Constants.UploadFor.animal
            }else if type == 2 {
                typeId = "accessId"
                forKey = Constants.UploadFor.accessories

            }else if type == 3 {
                typeId = "serviceId"
                forKey = Constants.UploadFor.service

            }
            var params = [String: Any]()
            params[SocketKeys.senderId] = String(userId)
            params[SocketKeys.receiverId] = String(friendId)
            params[typeId] = String(animalId)

            switch currentMessageType {
                
            case .textWithSocket:
                params[SocketKeys.message] = message
                params[SocketKeys.type] = "1"
                SocketHelper.shared.sendChat(params: params)
            case .imageToServer:
                params[SocketKeys.message] = message
                params[SocketKeys.type] = "2"
                params[ServerKeys.forKey] = forKey
                params[ServerKeys.type] = ServerKeys.image
                if let data = image?.jpegData(compressionQuality: 1) {
                    params[ServerKeys.photos] = [data]
                    requestServer(params: params)
                }
            case .imageUrlWithSocket:
                params[SocketKeys.message] = message
                params[SocketKeys.type] = "2"
                params["url"] = imageUrl

                SocketHelper.shared.sendChat(params: params)

            }
           

        }

    }
}
extension MZChatVC{
    @objc func keyboardWillShow(sender: NSNotification) {
        if let userInfo = sender.userInfo {
            if let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue?.size.height {
                let margin: CGFloat = 5
                 constraintBottom.constant = -keyboardHeight
                if #available(iOS 11.0, *) {
                    constraintBottom.constant = -keyboardHeight + view.safeAreaInsets.bottom - margin
                }
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()

                }) { (completed) in
                    self.scrollToBottom(animated: true)

                }
        
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        constraintBottom.constant = 0.0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
       
    }
    
   
}
extension MZChatVC: MediaSelectedDelegate{
    func selectedMediaType(image: UIImage) {
        currentMessageType = .imageToServer
        sendToServer(message: "", image: image)
    }
}
