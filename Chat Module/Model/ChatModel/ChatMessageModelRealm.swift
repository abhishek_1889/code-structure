//
//  ChatMessageModelRealm.swift
//  MyZoo
//
//  Created by Vikas Sachan on 25/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//



import RealmSwift

class ChatMessageModelRealm:Object {
    @objc dynamic var animalId = 0
    @objc dynamic var zooType = 0
    @objc dynamic var url: String?
    @objc dynamic var timeStamp: String?
    @objc dynamic var userName: String?
    @objc dynamic var id = 0
    @objc dynamic var type = 0
    @objc dynamic var senderId = 0
    @objc dynamic var recieverId = 0
    @objc dynamic var message: String?
    @objc dynamic var isRead = 0

    convenience init(data: Dictionary<String, Any>?) {
        self.init()
        if let data = data {
            isRead = (data[SocketKeys.isRead] as? Int) ?? 0
            id = (data[SocketKeys.id] as? Int) ?? 0
            type = (data[SocketKeys.type] as? Int) ?? 0
            senderId = (data[SocketKeys.senderId] as? Int) ?? 0
            recieverId = (data[SocketKeys.receiverId] as? Int) ?? 0
            timeStamp = data[SocketKeys.timeStamp] as? String
            message = data[SocketKeys.message] as? String
            animalId = data[SocketKeys.animalId] as? Int ?? 0
            url = data[SocketKeys.url] as? String
            zooType = (data[SocketKeys.zooType] as? Int) ?? 0
            userName = data[SocketKeys.userName] as? String

        }
    }
    override static func primaryKey() -> String? {
        return SocketKeys.id
    }
   
}
