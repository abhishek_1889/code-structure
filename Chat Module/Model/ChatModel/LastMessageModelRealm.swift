//
//  LastMessageModelRealm.swift
//  MyZoo
//
//  Created by Vikas Sachan on 25/04/19.
//  Copyright © 2019 MobileCoderz. All rights reserved.
//

import RealmSwift

class LastMessageModelRealm:Object {
    @objc dynamic var animalId = 0
    @objc dynamic var zooType = 0
    @objc dynamic var url:String?
    @objc dynamic var timestamp = 0

    @objc dynamic var arabicName: String?
    @objc dynamic var breedName: String?
    @objc dynamic var fullName: String?
    @objc dynamic var lastMessage: String?
    @objc dynamic var uniqueId:String?
    @objc dynamic var badge = 0
    @objc dynamic var userId = 0
    convenience init(data: Dictionary<String, Any>?) {
        self.init()
        if let data = data {
            animalId = (data[SocketKeys.animalId] as? Int) ?? 0
            badge = (data[SocketKeys.badge] as? Int) ?? 0
            userId = (data[SocketKeys.userId] as? Int) ?? 0
            zooType = (data[SocketKeys.zooType] as? Int) ?? 0
            arabicName = data[SocketKeys.arabicName] as? String
            breedName = data[SocketKeys.breedName] as? String
            fullName = data[SocketKeys.fullName] as? String
            lastMessage = data[SocketKeys.lastMessage] as? String
            timestamp = Int(data[SocketKeys.timestamp] as? String ?? "0") ?? 0
            url = data[SocketKeys.url] as? String
            uniqueId = String(userId) + "-" + String(animalId)
        }
    }
    override static func primaryKey() -> String? {
        return SocketKeys.uniqueId
    }
}
