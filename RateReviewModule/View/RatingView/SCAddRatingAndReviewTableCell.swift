//
//  SCAddRatingAndReviewTableCell.swift
//  Shift
//
//  Created by Mobilecoderz1 on 18/07/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import UIKit

class SCAddRatingAndReviewTableCell: UITableViewCell {
    @IBOutlet weak var labelDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupCell(order: Order?){
        if order?.orderDetail?.status == .assigned {
           // labelDetail.text = "OTP: \(order?.orderDetail?.verifyOtp ?? "Missing")"
            labelDetail.text =  String(format: NSLocalizedString("Verification code: %@", comment: ""), order?.orderDetail?.verifyOtp ?? "N/A")

        }else if order?.orderDetail?.status == .completed {
            if order?.orderDetail?.isRated ?? "0" == "1" {
                labelDetail.text = "View your rating".localized()
            }else {
                labelDetail.text = "Rate & Review".localized()
            }
            
        }
    }
    
}
