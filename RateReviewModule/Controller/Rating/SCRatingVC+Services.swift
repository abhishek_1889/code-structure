//
//  SCRatingVC+Services.swift
//  Shift
//
//  Created by Mobilecoderz1 on 24/07/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import Foundation
extension SCRatingVC{
    func requestForGenericResponse(params: [String: Any]?, endPoint: String){
        labelPlaceholder.isHidden = true
        ApiManager.requestGenericServer(path: endPoint, parameters: params, methodType: .post, result:  { [weak self](model: Ratings) in
            self?.showData(model: model)
            
        }) { [weak self] (error) in
            if (self?.rating?.data?.data?.count ?? 0) > 0 {
                return
            }
            self?.labelPlaceholder.text = error.debugDescription
            self?.labelPlaceholder.isHidden = false
           
        }
    }
    func showData(model: Ratings){
        switch currentLoadingState {
        case .refresh:
            rating = model
            if rating?.data?.data?.count == nil {
                labelPlaceholder.isHidden = false
                labelPlaceholder.text = rating?.message
            }
            self.tableView.reloadData()
        case .nextPage:
            if let data = model.data?.data {
            rating?.data?.data?.append(contentsOf: data)
                self.tableView.reloadData()
            }
            rating?.data?.next_page_url = model.data?.next_page_url
        }
     
    }
}
