//
//  SCRatingVC+TableView.swift
//  Shift
//
//  Created by Mobilecoderz1 on 24/07/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import UIKit
import Kingfisher
extension SCRatingVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return techDetail == nil ? 0 : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  rating?.data?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.ratingDetailTableCell, for: indexPath) as? SCRatingDetailTableCell else {return UITableViewCell()}
        let rate = rating?.data?.data?[indexPath.row]
       cell.setupCell(rate: rate)
        return cell
    }
        
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.CellIdentifier.ratingTableHeaderFooterView) as? SCRatingTableHeaderFooterView
        view?.contentView.backgroundColor = UIColor.AppColor.appLightGray
        view?.setupData(detail: techDetail)
        return view
    }
}

extension SCRatingVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let total = rating?.total, let perPage = rating?.per_page, let nextUrl = rating?.next_page_url{
//
//
//        }
        guard let total = rating?.data?.total, let ratingCount = rating?.data?.data?.count else{
                return
        }
        if ratingCount >= total {
            return
        }
        if indexPath.row == ratingCount - 1 {
            if let nextUrl = rating?.data?.next_page_url{
                requestServer(endPoint: nextUrl, currentState: .nextPage)
            }
        }
        
    }
}
