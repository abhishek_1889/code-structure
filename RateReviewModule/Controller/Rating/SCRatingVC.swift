//
//  SCRatingVC.swift
//  Shift
//
//  Created by Mobilecoderz1 on 24/07/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import UIKit
enum LoadingState{
    case refresh
    case nextPage
}
class SCRatingVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelPlaceholder: UILabel!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.AppColor.appGreen
        return refreshControl
    }()
    var currentLoadingState: LoadingState = .refresh
    var rating: Ratings?
    var techDetail: TechnicianDetail?
    override func viewDidLoad() {
        super.viewDidLoad()

       setupNavigationBar()
        setupTableView()
        requestServer(endPoint: MethodName.ratingDetail, currentState: .refresh)
    }
    
    func setupNavigationBar(){
        navigationItem.setHidesBackButton(true, animated: false)
        setLeftBarButtonItem("Detail".localized(), andImage: UIImage(named: "white_back"))
        let gesture = UITapGestureRecognizer(target: self, action: #selector(buttonBackTapped))
        gesture.numberOfTapsRequired = 1
        navigationItem.leftBarButtonItem?.customView?.addGestureRecognizer(gesture)
    }
    @objc func buttonBackTapped() {
      navigationController?.popViewController(animated: true)
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        Console.log("handleRefresh")
        refreshControl.endRefreshing()
        requestServer(endPoint: MethodName.ratingDetail, currentState: .refresh)

    }
    func requestServer(endPoint: String, currentState: LoadingState){
        if let techId = techDetail?.techniciansId{
        currentLoadingState = currentState
            requestForGenericResponse(params: ["tech_id": techId], endPoint: endPoint)
        }
    }
    func setupTableView(){
        tableView.refreshControl = refreshControl
        tableView.register(UINib(nibName: Constants.CellIdentifier.ratingDetailTableCell, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.ratingDetailTableCell)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedSectionHeaderHeight = 300
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: Constants.CellIdentifier.ratingTableHeaderFooterView, bundle: nil), forHeaderFooterViewReuseIdentifier: Constants.CellIdentifier.ratingTableHeaderFooterView)
        tableView.tableFooterView = UIView()
    }
}
