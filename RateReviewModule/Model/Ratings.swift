//
//  Ratings.swift
//  Shift
//
//  Created by Mobilecoderz1 on 24/07/19.
//  Copyright © 2019 Mobilecoderz. All rights reserved.
//

import Foundation
struct Ratings: Decodable {
    let message: String?
    var data: RatingData?

}
struct RatingData: Codable {
    var data: [UsersRating]?
    var next_page_url: String?
    //    let first_page_url: String?
    let total: Int?
    //    let per_page: Int?
}
struct UsersRating: Codable {
    let name: String?
    let image: URL?
    let user_rate: String?
    let review: String?
    let time_stamp: String?
}
